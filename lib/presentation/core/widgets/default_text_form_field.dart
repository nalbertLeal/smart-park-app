import 'package:flutter/material.dart';

class DefaultTextFormField extends StatelessWidget {
  final bool obscureText;
  final int? maxLength;
  final String? hintText;
  final String? labelText;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;

  const DefaultTextFormField({
    this.maxLength,
    this.hintText,
    this.labelText,
    this.validator,
    this.onChanged,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.width -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;

    return Container(
      width: screenWidth * .9,
      height: screenHeight * .2,
      child: TextFormField(
        obscureText: obscureText,
        maxLength: maxLength,
        decoration: InputDecoration(
          hintText: hintText,
          labelText: labelText,
        ),
        validator: validator,
        onChanged: onChanged,
      ),
    );
  }
}
