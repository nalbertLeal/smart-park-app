import 'package:flutter/material.dart';

import '../../../../utils/responsive/device_screen_type.dart';

class SizingInformation {
  final DeviceScreenType deviceScreenType;
  final Size screenSize;
  final Size localWidgetSize;

  SizingInformation({
    this.deviceScreenType,
    this.screenSize,
    this.localWidgetSize,
  });

  @override
  String toString() {
    return 'Orientation: DeviceType:$deviceScreenType, ScreenSize:$screenSize, LocalWidgetSize:$localWidgetSize';
  }
}