import 'package:flutter/material.dart';

class DefaultRaisedButton extends StatelessWidget {
  final Widget? child;
  final void Function()? onPressed;

  const DefaultRaisedButton({
    this.child,
    this.onPressed
  }); 
  
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Container(
      height: height * .1,
      width: width * .6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
      ),
      child: RaisedButton(
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}