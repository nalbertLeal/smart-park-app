import "package:flutter/material.dart";

// instances
import "../../../utils/navigation.dart";
import "../../auth/pages/login_page.dart";
import '../../auth/pages/register_user_page.dart';
import '../widgets/default_raised_button.dart';

class MainPage extends StatelessWidget {
  static const String route = "/";

  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  width: width,
                  height: height * .3,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(width * .1),
                      bottomRight: Radius.circular(width * .1),
                    ),
                  ),
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: width * .2,
                    height: width * .2,
                    child: Column(
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Colors.white,
                          size: width * .1,
                        ),
                        Icon(
                          Icons.directions_car,
                          color: Colors.white,
                          size: width * .1,
                        ),
                      ],
                    ),
                  )),
              SizedBox(
                width: width,
                height: height * .25,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    DefaultRaisedButton(
                      onPressed: () => Navigation().navigatoTo(LoginPage.route),
                      child: const Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    DefaultRaisedButton(
                      onPressed: () => Navigation().navigatoTo(RegisterUserPage.route),
                      child: const Text(
                        "Register",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
