import "package:flutter/material.dart";
import 'package:flutter_hooks/flutter_hooks.dart';

// instances
import '../../../utils/navigation.dart';

class ParkPage extends HookWidget {
  static const String route = "/park-screen";

  const ParkPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    final parkingVague = useState(<Widget>[
      Positioned(
        child: Container(
          width: 55,
          height: 30,
          color: Colors.red,
        ),
      ),
    ]);

    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          child: Column(
            children: [
              SizedBox(
                width: width,
                height: height * .1,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigation().goBack();
                      },
                    ),
                    Text(
                      "Park: ",
                      // "Park: ${parkModel.name}",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: width,
                height: height * .9,
                child: InteractiveViewer(
                  constrained: false,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 1.5,
                        height: height * .9,
                        child: const FlutterLogo(),
                      ),
                      ...parkingVague.value,
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
