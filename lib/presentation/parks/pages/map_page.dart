import 'dart:async';
import 'dart:convert';

import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import 'package:webview_flutter/webview_flutter.dart';

// instances
import '../../../utils/navigation.dart';

class MapPage extends StatefulWidget {
  static const String route = "/map-screen";

  const MapPage({Key? key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  WebViewController? webviewController;
  late Timer locationTimer;
  bool mapHTMLLoaded = false;

  @override
  void initState() {
    super.initState();

    locationTimer = Timer.periodic(
      const Duration(
        milliseconds: 500,
      ),
      (timer) async {
        if (!timer.isActive || webviewController == null || !mapHTMLLoaded) {
          return;
        }

        final position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
        );

        addMarker(position.latitude, position.longitude);
      },
    );
  }

  @override
  void dispose() {
    super.dispose();

    locationTimer.cancel();
  }

  Future<void> loadMapFile() async {
    final file = await rootBundle.loadString('assets/html/map.html');
    webviewController!.loadUrl(
      Uri.dataFromString(
        file,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
      ).toString(),
    );

    // await setMap();
    // await setOSMTiles();
  }

  Future<void> setMap() async {
    webviewController!.evaluateJavascript('setMap()');
  }

  Future<void> setOSMTiles() async {
    await webviewController!.evaluateJavascript('setOSMTiles()');
  }

  Future<void> addMarker(double lat, double long) async {
    webviewController!.evaluateJavascript('addMarker($lat, $long)');
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          child: Column(
            children: [
              SizedBox(
                width: width,
                height: height * .1,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigation().goBack();
                      },
                    ),
                    Text(
                      "Park: ",
                      // "Park: ${parkModel.name}",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: width,
                height: height * .9,
                child: WebView(
                  initialUrl: 'about:blank',
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController wvc) async {
                    webviewController = wvc;
                    await loadMapFile();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
