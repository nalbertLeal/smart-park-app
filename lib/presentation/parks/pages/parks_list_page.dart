import "package:flutter/material.dart";
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../utils/navigation.dart';
import '../widgets/parks_list.dart';

class ParksListPage extends HookWidget {
  static const String route = "/parks-list-screen";

  final _scrollController = ScrollController();

  ParksListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              width: width,
              height: height * .1,
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigation().goBack();
                    },
                  ),
                  Text(
                    "Avaliable Parks",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
            ),
            Container(
              width: width,
              height: height * .1,
              alignment: Alignment.center,
              child: Text(
                "Select a park !",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  letterSpacing: .15,
                  fontFamily: "Roboto",
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              width: width,
              height: height * .8,
              child: ParksList(
                scrollController: _scrollController,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        },
        child: const Icon(Icons.arrow_upward),
      ),
    );
  }
}
