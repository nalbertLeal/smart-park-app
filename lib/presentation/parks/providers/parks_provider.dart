import 'package:flutter/material.dart';
import 'package:flutter_riverpod/all.dart';

import '../../../data/repositories/park_repository_impl.dart';
import '../../../domain/entities/park.dart';
import '../../../domain/use_cases/get_city_parks.dart';

final parksProvider = ChangeNotifierProvider((ref) => ParksProvider());

class ParksProvider extends ChangeNotifier {

  Future<List<Park>> getAllParks() async {
    final getCityParks = GetCityParks(ParkRepositoryImpl());
    return getCityParks.execute('natal');
  }
}