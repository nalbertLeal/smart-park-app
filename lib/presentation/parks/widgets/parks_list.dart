import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../domain/entities/park.dart';
import '../../../utils/navigation.dart';
import '../pages/park_page.dart';
import '../providers/parks_provider.dart';

class ParksList extends HookWidget {
  final ScrollController? scrollController;

  const ParksList({
    this.scrollController,
  });

  @override
  Widget build(BuildContext context) {
    final _parksProvider = useProvider(parksProvider);

    return FutureBuilder<List<Park>>(
      future: _parksProvider.getAllParks(),
      builder: (
        BuildContext ctx1,
        AsyncSnapshot<List<Park>> snapshot,
      ) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: SizedBox(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(),
            ),
          );
        }

        if (snapshot.data == null || snapshot.data!.isEmpty) {
          return const Center(
            child: Text(
              'No parks found',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          );
        }

        return ListView.builder(
          controller: scrollController,
          itemCount: snapshot.data!.length,
          itemBuilder: (BuildContext ctx, int index) {
            final park = snapshot.data![index];

            return ListTile(
              key: UniqueKey(),
              onTap: () {
                // Navigation().navigatoTo(MapPage.route);
                Navigation().navigatoTo(ParkPage.route);
              },
              leading: Icon(
                Icons.location_on,
                color: Theme.of(context).primaryColor,
              ),
              title: Text(
                park.name!,
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  letterSpacing: .15,
                  fontFamily: "Roboto",
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                "A very nice park number $index",
                style: const TextStyle(
                  letterSpacing: .15,
                  fontFamily: "Roboto",
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            );
          },
        );
      },
    );
  }
}
