import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/all.dart';

import '../../../utils/navigation.dart';
import '../../core/widgets/default_text_form_field.dart';
import '../../parks/pages/parks_list_page.dart';
import '../providers/register_provider.dart';

class RegisterForm extends HookWidget {
  Future<void> registerUser(
    BuildContext context,
    RegisterProvider registerLogic,
    ValueNotifier<GlobalKey<FormState>> _formKey,
  ) async {
    try {
      await registerLogic.register(_formKey.value);
      Navigation().navigatoTo(ParksListPage.route);
    } catch (e) {
      print(e);
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('Error: $e'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight =
        MediaQuery.of(context).size.width - MediaQuery.of(context).padding.top;

    final _formKey = useState(GlobalKey<FormState>());
    final registerLogic = useProvider(registerProvider);

    return Form(
      key: _formKey.value,
      child: Column(
        children: [
          DefaultTextFormField(
            hintText: 'Your name',
            labelText: 'Name',
            maxLength: 50,
            onChanged: (String value) => registerLogic.name = value,
            validator: (_) => null,
          ),
          Padding(
            padding: EdgeInsets.only(bottom: screenHeight * .05),
            child: DefaultTextFormField(
              hintText: 'Your email',
              labelText: 'Email',
              maxLength: 60,
              onChanged: (String value) => registerLogic.email = value,
              validator: (_) => registerLogic.validatePassword(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: screenHeight * .1),
            child: Stack(
              alignment: Alignment.centerRight,
              children: [
                DefaultTextFormField(
                  hintText: 'Your password',
                  labelText: 'Password',
                  maxLength: 15,
                  obscureText: !registerLogic.showPassword,
                  onChanged: (String value) => registerLogic.password = value,
                  validator: (_) => registerLogic.validatePassword(),
                ),
                IconButton(
                  color: Theme.of(context).primaryColor,
                  icon: const Icon(Icons.remove_red_eye),
                  onPressed: registerLogic.invertShowPasswordValue,
                )
              ],
            ),
          ),
          SizedBox(
            width: screenWidth * .8,
            height: screenHeight * .15,
            child: RaisedButton(
              onPressed: () => registerUser(context, registerLogic, _formKey),
              child: const Text("Register"),
            ),
          )
        ],
      ),
    );
  }
}
