import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/all.dart';

import '../../../utils/navigation.dart';
import '../../core/widgets/default_raised_button.dart';
import '../../core/widgets/default_text_form_field.dart';
import '../../parks/pages/parks_list_page.dart';
import '../providers/login_provider.dart';

class LoginForm extends HookWidget {
  Future<void> pressLoginButton(
    BuildContext context,
    LoginProvider loginLogic,
    ValueNotifier<GlobalKey<FormState>> _formKey,
  ) async {
    try {
      await loginLogic.login(_formKey.value);
      Navigation().navigatoTo(ParksListPage.route);
    } catch (e) {
      print(e);
      Scaffold.of(context).showSnackBar(
        const SnackBar(
          content: Text('Error'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight =
        MediaQuery.of(context).size.width - MediaQuery.of(context).padding.top;

    final _formKey = useState(GlobalKey<FormState>());
    final loginProv = useProvider(loginProvider);

    return Form(
      key: _formKey.value,
      child: Column(
        children: [
          DefaultTextFormField(
            hintText: 'Your email',
            labelText: 'Email',
            maxLength: 60,
            onChanged: (value) => loginProv.email = value,
            validator: (_) => loginProv.validateEmail(),
          ),
          Stack(
            alignment: Alignment.centerRight,
            children: [
              DefaultTextFormField(
                hintText: 'Write your password',
                labelText: 'Password',
                maxLength: 30,
                obscureText: loginProv.showPassword,
                onChanged: (value) => loginProv.password = value,
                validator: (_) => loginProv.validatePassword(),
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: const Icon(Icons.remove_red_eye),
                onPressed: loginProv.invertShowPasswordValue,
              )
            ],
          ),
          Container(
            width: screenWidth * .8,
            height: screenHeight * .2,
            padding: EdgeInsets.only(top: screenWidth * .05),
            child: DefaultRaisedButton(
              onPressed: () => pressLoginButton(context, loginProv, _formKey),
              child: const Text("Login"),
            ),
          ),
        ],
      ),
    );
  }
}
