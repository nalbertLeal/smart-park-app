import "package:flutter/material.dart";

import '../../../utils/navigation.dart';
import '../widgets/login_form.dart';

class LoginPage extends StatelessWidget {
  static const String route = "/login-screen";

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: width,
                height: height * .1,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigation().goBack();
                      },
                    ),
                    Text(
                      "Login",
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: width * .8,
                height: height * .9,
                child: LoginForm(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
