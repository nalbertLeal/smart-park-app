import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../data/repositories/user_repository_impl.dart';
import '../../../domain/entities/user.dart';
import '../../../domain/entities/value_objects/email.dart';
import '../../../domain/entities/value_objects/password.dart';
import '../../../domain/use_cases/login.dart';
import '../../../exceptions/invalid_user.dart';

const String passwordValue = '';
final passwordProvider = Provider((ref) => passwordValue);

final loginProvider = ChangeNotifierProvider((ref) => LoginProvider());

class LoginProvider extends ChangeNotifier {
  String email = '';
  String password = '';
  bool showPassword = true;

  String? validateEmail() {
    final isEmailValid = Email(email).isValid();
    if (isEmailValid) {
      return null;
    } else {
      return 'Invalid Email';
    }
  }

  String? validatePassword() {
    final isPasswordValid = Password(password).isValid();
    if (isPasswordValid) {
      return null;
    } else {
      return 'Invalid Email';
    }
  }

  void invertShowPasswordValue() {
    showPassword = !showPassword;
    notifyListeners();
  }

  void resetProvider() {
    email = '';
    password = '';
    showPassword = false;
  }

  Future<void> login(GlobalKey<FormState> formState) async {
    final isValid = formState.currentState!.validate();
    if (!isValid) throw InvalidUser();
    
    final User user = User(
      email: email,
      password: password,
    );

    final Login loginUseCase = Login(UserRepositoryImpl());
    await loginUseCase.execute(user);
  }
}
