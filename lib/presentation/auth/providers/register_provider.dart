import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../data/repositories/user_repository_impl.dart';
import '../../../domain/entities/user.dart';
import '../../../domain/entities/value_objects/email.dart';
import '../../../domain/entities/value_objects/password.dart';
import '../../../domain/use_cases/register.dart';
import '../../../exceptions/invalid_user.dart';

final registerProvider = ChangeNotifierProvider((ref) => RegisterProvider());

class RegisterProvider extends ChangeNotifier {
  String name = '';
  String email = '';
  String password = '';
  bool showPassword = false;

  String? validateEmail() {
    final isEmailValid = Email(email).isValid();
    if (isEmailValid) {
      return null;
    } else {
      return 'Invalid Email';
    }
  }

  String? validatePassword() {
    final isPasswordValid = Password(password).isValid();
    if (isPasswordValid) {
      return null;
    } else {
      return 'Invalid Email';
    }
  }

  void invertShowPasswordValue() {
    showPassword = !showPassword;
    notifyListeners();
  }

  void resetProvider() {
    name = '';
    email = '';
    password = '';
    showPassword = false;
  }

  Future<void> register(GlobalKey<FormState> formState) async {
    final isValid = formState.currentState!.validate();
    if (!isValid) throw InvalidUser();
    
    final User user = User(
      name: name,
      email: email,
      password: password,
    );
    final loginUseCase = Register(UserRepositoryImpl());
    await loginUseCase.execute(user);
  }
}
