import 'package:equatable/equatable.dart';

class Park extends Equatable {
  final String? id;
  final String? name;
  final String? country;
  final String? state;
  final String? city;
  final int? size;
  final DateTime createdAt;

  Park({
    this.id = '',
    this.name = '',
    this.country = '',
    this.state = '',
    this.city = '',
    // this.location,
    this.size = 0,
    DateTime? createdAt,
  }) : this.createdAt = (createdAt ?? DateTime.now());

  @override
  List<Object?> get props => [id, name, country, state, city];
}