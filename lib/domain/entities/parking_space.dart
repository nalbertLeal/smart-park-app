import 'package:equatable/equatable.dart';

class ParkingSpace extends Equatable {
  final String? parkId;
  final int? position;
  final bool? free;
  final DateTime updatedAt;
  final DateTime createdAt;

  ParkingSpace({
    this.parkId,
    this.position,
    this.free,
    DateTime? updatedAt,
    DateTime? createdAt,
  })  : this.updatedAt = (updatedAt ?? DateTime.now()),
        this.createdAt = (createdAt ?? DateTime.now());

  @override
  List<Object?> get props => [parkId, position, free];
}
