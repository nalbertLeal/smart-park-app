import 'package:equatable/equatable.dart';

import './value_objects/email.dart';
import './value_objects/password.dart';

class User extends Equatable {
  final String? name;
  final String? email;
  final String? password;
  final DateTime createdAt;

  User({
    this.name = '',
    this.email,
    this.password,
    DateTime? createdAt,
  }) : this.createdAt = (createdAt ?? DateTime.now());

  bool get isValidName => name!.length < 100 || name!.isEmpty;

  bool get isValid {
    return isValidName && Email(email).isValid() &&
        Password(password).isValid();
  }

  bool get isNotValid {
    return !isValid;
  }

  @override
  List<Object?> get props => [email];
}
