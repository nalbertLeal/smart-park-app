class Password {
  final String? _password;

  Password(this._password);

  String? get password => _password;

  bool isValid() {
    return _password!.length > 7 && _password!.length < 21;
  } 
}