class Email {
  final String? _email;

  Email(this._email);

  String? get email => _email;

  bool isValid() {
    final validator = RegExp(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)");

    return email!.isNotEmpty && validator.hasMatch(_email!);
  }
}
