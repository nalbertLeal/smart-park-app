import '../entities/user.dart';

abstract class UserRepositoryInter {
  Future<bool> get isAuthenticated;
  Future<User> get getCurrentUser;
  Future<User> login(User user);
  Future<User> register(User user);
  Future<User> logout();
}