import '../entities/park.dart';

abstract class ParkRepositoryInter {
  Future<List<Park>> getAllParks();
  Future<Park> getParkById(String id);
  Future<List<Park>> getCityParks(String city);
}