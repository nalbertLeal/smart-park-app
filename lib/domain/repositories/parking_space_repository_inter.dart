import '../entities/parking_space.dart';

abstract class ParkingSpaceRepositoryInter {
  Future<List<ParkingSpace>> getAllParkingSpaceByParkId(String parkId);
}