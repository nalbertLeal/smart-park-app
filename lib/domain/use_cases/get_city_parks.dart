import '../entities/park.dart';
import '../repositories/park_repository_inter.dart';

class GetCityParks {
  final ParkRepositoryInter parkRepository;

  GetCityParks(this.parkRepository);

  Future<List<Park>> execute(String city) async {
    return parkRepository.getAllParks();
  }
}