import '../../exceptions/invalid_user.dart';
import '../entities/user.dart';
import '../repositories/user_repository_inter.dart';

class Register {
  final UserRepositoryInter userRepository;

  Register(this.userRepository);

  Future<void> execute(User user) async {
    if (user.isNotValid) throw InvalidUser();
    await userRepository.register(user);
  }
}