import '../../exceptions/invalid_user.dart';
import '../entities/user.dart';
import '../repositories/user_repository_inter.dart';

class Login {
  final UserRepositoryInter userRepository;

  Login(this.userRepository);

  Future<void> execute(User user) async {
    if (user.isNotValid) throw InvalidUser();
    await userRepository.login(user);
  }
}