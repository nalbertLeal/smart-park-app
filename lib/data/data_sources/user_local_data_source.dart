import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../domain/entities/user.dart';
import '../../exceptions/no_user_locally_saved.dart';
import '../models/user_models.dart';

class UserLocalDataSource {
  Future<User> get currentUser async {
    try {
      final db = await SharedPreferences.getInstance();
      final userJSON = db.getString('current_user')!;
      final userMap = json.decode(userJSON) as Map;
      return UserModel.fromMap(userMap);   
    } catch (_) {
      throw NoUserLocallySaved();
    }
  }

  Future<void> storeUserLocaly(UserModel user) async {
    final db = await SharedPreferences.getInstance();
    final userJSON = json.encode(user.toMap());
    await db.setString('current_user', userJSON);
  }

  Future<User> logout() async {
    final db = await SharedPreferences.getInstance();
    final userJSON = db.getString('current_user')!;
    final userMap = json.decode(userJSON) as Map;
    final user = UserModel.fromMap(userMap);   
    await db.remove('current_user');
    return user;
  }
}
