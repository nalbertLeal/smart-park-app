import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart' as dot_env;
import 'package:http/http.dart' as http;

import '../../domain/entities/parking_space.dart';
import '../../exceptions/data_source_error.dart';
import '../models/parking_space_model.dart';
import './token_local_data_source.dart';

class ParkingSpaceApiDataSource {
  String? baseURL = dot_env.env['API_URL_BASE'];

  ParkingSpaceApiDataSource({this.baseURL});

  Future<List<ParkingSpace>> getAllParkingSpaceByParkId(String parkId) async {
    final url = Uri.parse('$baseURL/vacancies/$parkId');
    try {
      final tokenLocalDataSource = TokenLocalDataSource();
      final token = tokenLocalDataSource.currentToken;

      final response = await http.post(
        url,
        headers: {
          'authorization': 'Bearer $token',
        },
      );

      if (response.statusCode != 200) throw DataSourceError();
      final body = jsonDecode(response.body) as List<Map>;

      return body.map((e) => ParkingSpaceModel.fromMap(e)).toList();
    } on http.ClientException {
      throw DataSourceError();
    }
  }
}