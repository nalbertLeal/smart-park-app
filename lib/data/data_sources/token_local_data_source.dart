import 'package:shared_preferences/shared_preferences.dart';

class TokenLocalDataSource {
  Future<String?> get currentToken async {
    final db = await SharedPreferences.getInstance();
    return db.getString('token');
  }

  Future<void> storeToken(String token) async {
    final db = await SharedPreferences.getInstance();
    await db.setString('token', token);
  }

  Future<void> deleteToken(String token) async {
    final db = await SharedPreferences.getInstance();
    await db.remove('token');
  }
}