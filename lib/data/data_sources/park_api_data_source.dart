import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart' as dot_env;
import 'package:http/http.dart' as http;

import '../../domain/entities/park.dart';
import '../../exceptions/data_source_error.dart';
import '../models/park_model.dart';
import './token_local_data_source.dart';

class ParkAPIDataSource {
  String? baseURL = dot_env.env['API_URL_BASE'];

  // ParkAPIDataSource({
  //   this.baseURL,
  // });

  Future<String?> get token async {
    final tokenLocalDataSource = TokenLocalDataSource();
    return tokenLocalDataSource.currentToken;
  }

  Park _handleResponse(http.Response response) {
    if (response.statusCode != 200) throw DataSourceError();
    final body = jsonDecode(response.body) as Map<String, Map>;

    return ParkModel.fromMap(body['user']!);
  }

  Future<List<Park>> getAllPark() async {
    final url = Uri.parse('$baseURL/parks');
    try {
      final response = await http.post(
        url,
        headers: {
          'authorization': 'Bearer ${await token}',
        },
      );

      if (response.statusCode != 200) throw DataSourceError();
      final body = jsonDecode(response.body) as Map;

      final List<Park> parks = [];

      body.forEach((k, v) {
        final p = ParkModel(
          size: v['size'] as  int?,
          id: v['_id'] as String?,
          createdAt: DateTime.parse(v['createdAt'] as String),
        );
        parks.add(p);
      });

      return parks;
    } on http.ClientException {
      throw DataSourceError();
    }
  }

  Future<Park> getParkById(String id) async {
    final url = Uri.parse('$baseURL/parks/$id');
    try {
      final tokenLocalDataSource = TokenLocalDataSource();
      final token = tokenLocalDataSource.currentToken;

      final response = await http.post(
        url,
        headers: {
          'authorization': 'Bearer $token',
        },
      );

      return _handleResponse(response);
    } on http.ClientException {
      throw DataSourceError();
    }
  }

  Future<List<Park>> getCityParks(String city) async {
    final url = Uri.parse('$baseURL/parks/city/$city');
    try {
      final tokenLocalDataSource = TokenLocalDataSource();
      final token = tokenLocalDataSource.currentToken;

      final response = await http.post(
        url,
        headers: {
          'authorization': 'Bearer $token',
        },
      );

      if (response.statusCode != 200) throw DataSourceError();
      final body = jsonDecode(response.body) as Map;

      return (body['parks'] as List<Map>).map((e) {
        return ParkModel.fromMap(e);
      }).toList();
    } on http.ClientException {
      throw DataSourceError();
    }
  }
}
