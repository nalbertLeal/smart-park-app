import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart' as dot_env;
import 'package:http/http.dart' as http;

import './token_local_data_source.dart';
import './user_local_data_source.dart';
import '../../domain/entities/user.dart';
import '../../exceptions/data_source_error.dart';
import '../../exceptions/login_problem.dart';
import '../../exceptions/user_already_exists.dart';
import '../models/user_models.dart';

class UserAPIDataSource {
  String? baseURL = dot_env.env['API_URL_BASE'];

  UserAPIDataSource({this.baseURL});

  Future<UserModel> _handleAuthResponse(
    http.Response response,
    Function handleError,
  ) async {
    if (response.statusCode != 200) throw Error();
    final body = jsonDecode(response.body) as Map;

    _treatHttpResponseError(body, handleError);
    
    _storeTokenInLocalDB(body);

    final user = UserModel.fromMap(body['user'] as Map);
    
    await _storeUserInLocalDB(user);

    return user;
  }

  void _treatHttpResponseError(Map body, Function handleError) {
    final errorResponse = body.containsKey('error');
    if (errorResponse) handleError();
  }

  void _storeTokenInLocalDB(Map body) {
    final tokenLocalDataSource = TokenLocalDataSource();
    tokenLocalDataSource.storeToken(body['token'] as String);
  }

  Future<void> _storeUserInLocalDB(UserModel user) async {
    final userLocalDataSource = UserLocalDataSource();
    await userLocalDataSource.storeUserLocaly(user);
  }

  Future<User> register(User user) async {
    final url = Uri.parse('$baseURL/auth/register');
    try {
      final response = await http.post(
        url,
        body: {
          'name': user.name,
          'email': user.email,
          'password': user.password,
        },
      );

      return _handleAuthResponse(
        response,
        () {
          throw UserAlreadyExists();
        },
      );
    } on http.ClientException {
      throw DataSourceError();
    }
  }

  Future<User> login(User user) async {
    final url = Uri.parse('$baseURL/auth/authenticate');
    try {
      final response = await http.post(
        url,
        body: {
          'email': user.email,
          'password': user.password,
        },
      );
      return _handleAuthResponse(
        response,
        () {
          throw LoginProblem();
        },
      );
    } on http.ClientException {
      throw DataSourceError();
    }
  }
}
