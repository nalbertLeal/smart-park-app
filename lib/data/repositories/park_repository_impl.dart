import '../../domain/entities/park.dart';
import '../../domain/repositories/park_repository_inter.dart';
import '../../exceptions/no_internet_connection.dart';
import '../../utils/is_connected.dart';
import '../data_sources/park_api_data_source.dart';

class ParkRepositoryImpl extends ParkRepositoryInter {
  final _parkAPIDataSource = ParkAPIDataSource();

  @override
  Future<Park> getParkById(String id) async {
    if (await isDeviceConnected) {
      return _parkAPIDataSource.getParkById(id);
    } else {
      throw NoInternetConnection();
    }
  }
  
  @override
  Future<List<Park>> getAllParks() async {
    if (await isDeviceConnected) {
      return _parkAPIDataSource.getAllPark();
    } else {
      throw NoInternetConnection();
    }
  }

  @override
  Future<List<Park>> getCityParks(String city) async {
    if (await isDeviceConnected) {
      return _parkAPIDataSource.getCityParks(city);
    } else {
      throw NoInternetConnection();
    }
  }
}
