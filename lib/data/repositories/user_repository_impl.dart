import 'package:flutter_dotenv/flutter_dotenv.dart' as dot_env;

import '../../domain/entities/user.dart';
import '../../domain/repositories/user_repository_inter.dart';
import '../../exceptions/no_internet_connection.dart';
import '../../exceptions/no_token_locally_saved.dart';
import '../../utils/is_connected.dart';
import '../data_sources/token_local_data_source.dart';
import '../data_sources/user_api_data_source.dart';
import '../data_sources/user_local_data_source.dart';

class UserRepositoryImpl extends UserRepositoryInter {
  final userAPIDataSource = UserAPIDataSource(
    baseURL: dot_env.env['API_URL_BASE'],
  );
  final userLocalDataSource = UserLocalDataSource();
  final tokenLocalDataSource = TokenLocalDataSource();

  @override
  Future<bool> get isAuthenticated async {
    try {
      tokenLocalDataSource.currentToken;
      return true;
    } on NoTokenLocallySaved {
      return false;
    }
  }

  @override
  Future<User> get getCurrentUser async {
    return userLocalDataSource.currentUser;
  }

  @override
  Future<User> login(User user) async {
    if (await isDeviceConnected) {
      return userAPIDataSource.login(user);
    } else {
      throw NoInternetConnection();
    }
  }

  @override
  Future<User> register(User user) async {
    if (await isDeviceConnected) {
      return userAPIDataSource.register(user);
    } else {
      throw NoInternetConnection();
    }
  }

  @override
  Future<User> logout() async {
    if (await isDeviceConnected) {
      return userLocalDataSource.logout();
    } else {
      throw NoInternetConnection();
    }
  }
}
