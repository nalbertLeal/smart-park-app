import '../../domain/entities/parking_space.dart';
import '../../domain/repositories/parking_space_repository_inter.dart';
import '../../exceptions/no_internet_connection.dart';
import '../../utils/is_connected.dart';
import '../data_sources/parking_space_api_data_source.dart';

class ParkingSpaceRepositoryImpl implements ParkingSpaceRepositoryInter {
  final _parkingSpaceApiDataSource = ParkingSpaceApiDataSource();

  @override
  Future<List<ParkingSpace>> getAllParkingSpaceByParkId(String parkId) async {
    if (await isDeviceConnected) {
      return _parkingSpaceApiDataSource.getAllParkingSpaceByParkId(parkId);
    } else {
      throw NoInternetConnection();
    }
  }
}