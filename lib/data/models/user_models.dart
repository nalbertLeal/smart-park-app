import '../../domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    String? name,
    String? email,
    String? password,
    String createdAt = ''
  }) : super(
    name: name,
    email: email,
    password: password,
    createdAt: DateTime.parse(createdAt) 
  );

  UserModel.fromMap(Map m) : super(
    name: m['name'] as String?,
    email: m['email'] as String?,
    password: (m['password'] ?? '') as String,
    createdAt: DateTime.parse(m['createdAt'] as String) 
  );

  Map toMap() {
    return {
      'name': name,
      'email': email,
      'password': password,
      'createdAt': createdAt.toIso8601String(),
    };
  }

  @override
  String toString() {
    return toMap().toString();
  }
}