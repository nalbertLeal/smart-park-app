import '../../domain/entities/park.dart';

class ParkModel extends Park {
  ParkModel({
    String? id,
    String? name,
    String? country,
    String? state,
    String? city,
    int? size,
    DateTime? createdAt,
  }) : super(
          id: id,
          name: name,
          country: country,
          state: state,
          city: city,
          size: size,
          createdAt: createdAt,
        );

  ParkModel.fromMap(Map m)
      : super(
          id: (m['_id'] ?? '') as String,
          name: (m['name'] ?? '') as String,
          country: (m['country'] ?? '') as String,
          state: (m['state'] ?? '') as String,
          city: (m['city'] ?? '') as String,
          size: (m['size'] as int?) ?? 0,
          createdAt: DateTime.parse((m['createdAt'] ?? DateTime.now().toIso8601String()) as String),
        );

  Map toMap() {
    return {
      '_id': id,
      'name': name,
      'country': country,
      'state': state,
      'city': city,
      'size': '$size',
      'createdAt': createdAt.toIso8601String(),
    };
  }
}
