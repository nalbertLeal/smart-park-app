import '../../domain/entities/parking_space.dart';

class ParkingSpaceModel extends ParkingSpace {
  ParkingSpaceModel({
    String? parkId,
    int? position,
    bool? free,
    DateTime? updatedAt,
    DateTime? createdAt,
  }) : super(
    parkId: parkId,
    position: position,
    free: free,
    updatedAt: updatedAt,
    createdAt: createdAt,
  );

  ParkingSpaceModel.fromMap(Map m) : super(
    parkId: m['parkId'] as String?,
    position: int.parse(m['position'] as String),
    free: m['free'] as bool?,
    updatedAt: DateTime.parse(m['updatedAt'] as String),
    createdAt: DateTime.parse(m['createdAt'] as String),
  );

  Map toMap() {
    return {
      'parkId': parkId,
      'position': position,
      'free': free,
      'updatedAt': updatedAt,
      'createdAt': createdAt.toIso8601String(),
    };
  }
}