import 'dart:io';

///    Informe if the device is connected to the internet. To this this 
/// function make a http request to google and if don't receive a response
/// of the status code family 400 then is connected to the internet.
Future<bool> get isDeviceConnected async {
  try {
    final result = await InternetAddress.lookup('www.google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
} 