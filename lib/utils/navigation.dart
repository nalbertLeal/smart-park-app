import "package:flutter/material.dart";

class Navigation {
  static Navigation? _instance;
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  factory Navigation() {
    return Navigation._instance ??= Navigation._internal();
  }

  Navigation._internal();

  Future<dynamic> navigatoTo(String routeName) {
    return navigatorKey.currentState!.pushNamed(routeName);
  }

  Future<dynamic> substitute(String routeName) {
    return navigatorKey.currentState!.popAndPushNamed(routeName);
  }

  void goBack() {
    navigatorKey.currentState!.pop();
  }
}