import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as dot_env;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:path_provider/path_provider.dart';

import 'main_widget.dart';

Future<void> initDotEnv() async {
  await dot_env.load();
}

// Future<String> currentPath() async {
//   final appDocDirectory = await getApplicationDocumentsDirectory();
//   final directory = await Directory('${appDocDirectory.path}/dir').create(recursive: true);
//   return directory.path; 
// }

// Future<void> initHiveDB() async {
//   // final path = Directory.current.path;
//   final path = await currentPath();
//   Hive.init(path);

//   await Hive.openBox('auth');
//   await Hive.openBox('user');
// }

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDotEnv();

  runApp(
    ProviderScope(
      child: MainWidget(),
    ),
  );
}
