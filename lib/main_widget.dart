import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'presentation/auth/pages/login_page.dart';
import 'presentation/auth/pages/register_user_page.dart';
import 'presentation/core/pages/main_page.dart';
import 'presentation/parks/pages/map_page.dart';
import 'presentation/parks/pages/park_page.dart';
import 'presentation/parks/pages/parks_list_page.dart';
import 'utils/navigation.dart';

class MainWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Smart parking',
      navigatorKey: Navigation().navigatorKey,
      theme: ThemeData(
        primarySwatch: Colors.teal,
        textTheme: GoogleFonts.robotoTextTheme(
          Theme.of(context).textTheme,
        ),
        buttonTheme: const ButtonThemeData(
          buttonColor: Colors.teal,
          textTheme: ButtonTextTheme.primary
        )
      ),
      home: const MainPage(),
      routes: {
        LoginPage.route: (_) => const LoginPage(),
        RegisterUserPage.route: (_) => const RegisterUserPage(),
        ParksListPage.route: (_) => ParksListPage(),
        MapPage.route: (_) => const MapPage(),
        ParkPage.route: (_) => const ParkPage(),
      },
    );
  }
}